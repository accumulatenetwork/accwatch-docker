![](AccWatch.png)


# AccWatch-Docker - The AccWatch runner

This is the recommended method of running AccWatch.

#### Prerequisites

- A Linux server
- Recent LTS Ubuntu & Debian based distributions should work out of the box. Any distro with Docker support should work.

#### Install

Log in as root.

```
apt update && apt -y install git telnet net-tools
git clone https://gitlab.com/accumulatenetwork/monitoring/accwatch-docker.git accwatch
cd accwatch
```


#### Run & Configure

All operations are done, using the run command.

```
./accwatch
```

If you want to create a ssh login account that takes you directly to the AccWatch menu:

```
./accwatch createuser accwatch
```

This will create a login account 'accwatch'. The private keys from ~/.ssh/authorized_keys will be copied to /home/accwatch/.ssh/authorized_keys. Edit as necessary.

---


#### SSL certificates

A self-certificate is automatically generated on start-up.
If you provide a domain & e-mail address within AccWatch a certificate from LetsEncrypt will automatically obtained.
If you have an existing certificate or wish to use an alternative certificate provider, copy the `.crt` and `.key` files to the `./certs` directory. They will be read on start-up. `.pem` files are also compatible. Rename them to `.crt` and `.key` files respectively. Set up a cron job, to make sure the files are kept valid.  These files will be monitored for changes, and automatically reloaded.

