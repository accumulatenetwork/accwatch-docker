#!/bin/sh
# For use with Amazon Route53

export AWS_ACCESS_KEY_ID=
export AWS_SECRET_ACCESS_KEY=
DOMAIN=mydomain.com

install()
{
  snap install core
  snap refresh core
  snap install --classic certbot
  ln -s /snap/bin/certbot /usr/bin/certbot
  snap set certbot trust-plugin-with-root=ok
  snap install certbot-dns-route53  
}

command -v certbot &> /dev/null || install


certbot certonly \
  --webroot-path $PWD \
  --dns-route53 \
  --non-interactive --agree-tos \
  -d $DOMAIN
  
cp /etc/letsencrypt/live/*/fullchain.pem domain.crt
cp /etc/letsencrypt/live/*/privkey.pem domain.key
