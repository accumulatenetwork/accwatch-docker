#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi


if command -v docker &> /dev/null
then
    echo "============================================================="
    echo "WARNING:  Docker is already installed on this server."
    echo "This script may make unwanted changes to Docker."
    echo ""
    read -p "Continue installation? " -n 1 -r
     if [[ $REPLY =~ ^[Nn]$ ]]
     then
        echo ""
        exit 1
     fi 
fi


# Uninstall old versions
apt-get remove docker docker-engine docker.io containerd runc

apt-get update
apt-get -y install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release


# Add Docker’s official GPG key
mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg


echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

apt-get update

apt-get -y install docker-ce docker-ce-cli containerd.io docker-compose-plugin

echo "Done"